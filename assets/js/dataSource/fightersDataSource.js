import { fighterService } from '../services/fighterService';

class FightersDataSource {
    fightersDetailsMap = new Map();

    async getFighterById(id) {
        let detailedFighter;

        if (this.fightersDetailsMap.has(id)) {
            detailedFighter = this.fightersDetailsMap.get(id);
        } 
        else {
            detailedFighter = await fighterService.getFighterDetails(id);
            this.fightersDetailsMap.set(id, detailedFighter);
        }

        return detailedFighter;
    }
}

export const fightersDataSource = new FightersDataSource();