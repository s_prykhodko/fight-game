class Fighter {
    constructor(obj) {
        this.name = obj.name;
        this.fullHealth = obj.health;
        this.health = obj.health;
        this.attack = obj.attack;
        this.defense = obj.defense;
    }

    getHitPower() {
        const criticalHitChance = Math.random() + 1;
        return this.attack * criticalHitChance;
    }

    getBlockPower() {
        const dodgeChance = Math.random() + 1;
        return this.defense * dodgeChance;
    }
}

export default Fighter;