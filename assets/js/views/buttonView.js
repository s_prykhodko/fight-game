import View from './view';

class ButtonView extends View {
    constructor(callback) {
        super();
        this.createButton(callback);
    }

    createButton(callback) {
        this.element = this.createElement({ tagName: 'button', className: 'unavailable'});
        this.element.classList.add('tingle-btn');
        this.element.classList.add('tingle-btn--danger');
        this.element.innerText = 'LET`S FIGHT';
        this.element.addEventListener('click', () => this.showGame(callback), false);
    }

    showGame(callback) {
        const checkboxes = document.getElementsByClassName('check');
        const checkedIds = [];
        for (let i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].checked) {
                checkedIds.push(`${i + 1}`);
            }
        }
        callback(checkedIds);
    }
}
  
export default ButtonView;
