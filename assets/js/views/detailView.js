import View from './view';
import Modal from './modalView';
import { fightersDataSource } from '../dataSource/fightersDataSource';


class DetailView extends View { 
    constructor(fighter) {
        super();
        this.modal = Modal.createModal(['button', 'overlay', 'escape']);
        this.show(fighter);
    }

    show(fighter) {
        this.modal.setContent(this.createForm(fighter));
        this.modal.addFooterBtn('Edit', 'tingle-btn tingle-btn--primary', () => {
            this.handleButton();
            this.modal.destroy();
        });
        this.modal.open();
    }

    handleButton() {
        const inputs = document.querySelectorAll("input[type='text']");
        const updatedFighter = {};
        for (let i = 0; i < inputs.length; i++) {
            updatedFighter[inputs[i].name] = (inputs[i].value);
        }
        fightersDataSource.fightersDetailsMap.set(updatedFighter._id, updatedFighter);
        document.getElementsByClassName('fighter')[updatedFighter._id - 1].querySelector('span').innerText = updatedFighter.name;
    }

    createForm(obj) {
        this.element = this.createElement({ tagName: 'div', className:'form' });
        Object.keys(obj).forEach(key => {
            if (!(key === '_id'|| key === 'source')) {
                this.element.append(this.createInput(key, obj[key]));
            }
            else {  
                const noInput = this.createInput(key, obj[key]);
                noInput.classList.add("non-edit");
                this.element.append(noInput);
            }
        });
        return this.element;
    }

    createInput(name, value) {
        const input = this.createElement({ tagName: 'div', className: name });
        const attributes = { id:name ,type: 'text', name, value };
        const inputField = this.createElement({ tagName: 'input', className:'input', attributes});
        const labelAttr = { for:name };
        const label = this.createElement({ tagName: 'label', className:name, labelAttr});

        label.innerText = name + ': ';
        input.append(label, inputField);
        return input;
    }

    createButton() {
        const editBtn = this.createElement({ tagName:'button', className:'edit'});
        editBtn.innerText = 'Edit';
        return editBtn;
    }
}

export default DetailView;
