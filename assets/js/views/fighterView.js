import View from './view';

class FighterView extends View {
    constructor(fighter, handleClick) {
        super();
        this.createFighter(fighter, handleClick);
    }
  
    createFighter(fighter, handleClick) {
        const { name, source } = fighter;
        this.nameElement = this.createName(name);
        this.imageElement = this.createImage(source);

        this.element = this.createElement({ tagName: 'div', className: 'fighter' });
    }
  
    createName(name) {
        const nameElement = this.createElement({ tagName: 'span', className: 'name' });
        nameElement.innerText = name;
        return nameElement;
    }
  
    createImage(source) {
        const attributes = { src: source };
        const imgElement = this.createElement({
            tagName: 'img',
            className: 'fighter-image',
            attributes
        });
        return imgElement;
    }
}

export default FighterView;