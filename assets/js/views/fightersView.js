import View from './view';
import RootFighterView from './rootFighterView';
import { fightersDataSource } from '../dataSource/fightersDataSource';
import DetailView from './detailView';

class FightersView extends View {
    constructor(fighters) {
        super();
        this.handleClick = this.handleFighterClick.bind(this);
        this.createFighters(fighters);
    }
    
    createFighters(fighters) {
        const fighterElements = fighters.map(fighter => {
            const fighterView = new RootFighterView(fighter, this.handleClick);
            return fighterView.element;
        });

        this.element = this.createElement({ tagName: 'div', className: 'fighters' });
        this.element.append(...fighterElements);
    }
  
    async handleFighterClick(event, fighter) {
        try {
            let detailedFighter = await fightersDataSource.getFighterById(fighter._id);
            new DetailView(detailedFighter);
        } catch (error) {
            console.warn(error);
            App.rootElement.innerText = 'Failed to load details';
        }
    }
}

export default FightersView;
