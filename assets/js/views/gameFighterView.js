import FighterView from './fighterView';

class GameFighterView extends FighterView { 
    constructor(fighter, handleClick) {
        super(fighter, handleClick);
    }

    createFighter(fighter, handleClick) {
        super.createFighter(fighter, handleClick);

        const healthElement = this.createHealthIndicator();
        this.element.append(this.nameElement, healthElement, this.imageElement);
        this.imageElement.addEventListener('click', event => handleClick(event, fighter), false);
    }

    createHealthIndicator() {
        const healthElement = this.createElement({ tagName: 'div', className: 'outerIndicator' });
        const indicator = this.createElement({ tagName: 'div', className: 'indicator' });
        
        const percentage = this.createElement({ tagName: 'span', className: 'percentage' });
        healthElement.append(percentage);
        healthElement.append(indicator);
        return healthElement;
    }
}

export default  GameFighterView ;