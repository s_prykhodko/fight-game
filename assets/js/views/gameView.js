import Fighter from '../fighter';
import GameFighterView from './gameFighterView';
import Modal from './modalView'
import View from './view';
import { fightersDataSource } from '../dataSource/fightersDataSource'
 
class GameView extends View {
    constructor(first, second, gameEl) {
        super();
        this.gameEl = gameEl;
        this.setupView(first, second);
        this.modal = Modal.createModal([]);
    }

    async setupView(id1, id2) {
        this.gameEl.classList.remove('hidden');

        const fighterObj1 = await fightersDataSource.getFighterById(id1);   
        const fighterObj2 = await fightersDataSource.getFighterById(id2);

        const fighter1 = new Fighter(fighterObj1);
        const fighter2 = new Fighter(fighterObj2);

        const fighterView1 = new GameFighterView(fighterObj1, () => this.handleFighterClick(fighter2, fighter1, fighterView1)).element;
        const fighterView2 = new GameFighterView(fighterObj2, () => this.handleFighterClick(fighter1, fighter2, fighterView2)).element;

        fighterView1.childNodes[1].firstChild.innerText = fighter1.health;
        fighterView2.childNodes[1].firstChild.innerText = fighter2.health;

        fighterView2.lastChild.classList.add('mirror');

        this.element = this.createElement({ tagName: 'div', className: 'fighters' });
        this.element.append(fighterView1, fighterView2);

        const message = this.createElement({ tagName: 'div', className: 'message' });
        message.innerText = 'Start the fight! Just click on the players! Let the strongest win !!!'
        this.gameEl.append(this.element, message);
    }

    handleFighterClick(attacker, attacked, view) {
        let damage = attacker.getHitPower() - attacked.getBlockPower();
        if (damage < 0) damage = 0;
        this.updateIndicator(attacker, attacked, damage, view);
        this.createMessage(attacker, damage);
    }

    createMessage(attacker, delta) {
        const message = document.querySelector('.message');
        if(delta === 0) {
            message.innerText = 'Hmm... You missed, ';
        }
        else if (delta < 1) {
            message.innerText = 'You beat like a girl, ';
        }
        else if (delta < 2) {
            message.innerText = 'Good blow, ';
        }
        else {
            message.innerText = "Right in the bull's eye, ";
        }
        message.innerText += attacker.name;
    }

    updateIndicator(attacker, attacked, delta, view){
        attacked.health -= delta;
        const indicator = view.childNodes[1].lastChild;
        const percents = view.childNodes[1].firstChild;
        const health = Math.round(attacked.health);
        const width = attacked.health / attacked.fullHealth * 100;

        if (health >= 0 ) {
            percents.innerText = health;
            indicator.style.width = width + '%';
        } else {
            percents.innerText = 0;
            indicator.style.width = '0%';
        }
        if (width <= 0) {
            this.showWinner(attacker);
        }
    }

    showWinner(winner) {
        this.modal.setContent('You are win ' + winner.name);
        this.modal.addFooterBtn('New Game', 'tingle-btn tingle-btn--primary', () => {
            this.newGame();
            this.modal.destroy();
        });
        this.modal.open();
    }

    newGame() {
        document.getElementById('game').innerHTML = '';
        document.querySelector('div#root button').classList.add('unavailable');
        const checkboxes = document.getElementsByClassName('check');
        for (let i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].checked) {
                checkboxes[i].checked = false;
            }
            else {
                checkboxes[i].parentNode.classList.remove('unavailable');
            }
        }
        document.getElementById('root').classList.remove('hidden');
    }
}

export default GameView;