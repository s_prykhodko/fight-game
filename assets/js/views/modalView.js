class Modal {
    static createModal(closeMethods) {
        return new tingle.modal({
            footer: true,
            stickyFooter: false,
            closeMethods: closeMethods,
            closeLabel: "Close",
            cssClass: ['custom-class-1', 'custom-class-2']
        });
    }
}

export default Modal;