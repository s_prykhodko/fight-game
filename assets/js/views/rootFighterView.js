import FighterView from './fighterView';

class RootFighterView extends FighterView {
    constructor(fighter, handleClick) {
        super(fighter, handleClick);
    }

    createFighter(fighter, handleClick) {
        super.createFighter(fighter, handleClick);
        this.element.append(this.imageElement, this.nameElement);
        const checkElement = this.createCheckbox();
        this.element.append(checkElement);
        this.element.addEventListener('click', event => handleClick(event, fighter), false);
    }

    createCheckbox() {
        const checkboxes = document.getElementsByClassName('check');
        const attributes = { type: 'checkbox' };
        const checkElement = this.createElement({
            tagName: 'input',
            className: 'check',
            attributes
        });
        checkElement.addEventListener('click', event => event.stopPropagation());
        checkElement.addEventListener('change', function() {
            const playBtn = document.querySelector('div#root button');

            if(this.checked) {
                let checkedCount = 0;
                for (let i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].checked) {
                        checkedCount += 1;
                    }
                }
                if (checkedCount === 2) {
                     playBtn.classList.remove('unavailable');
                    for (let i = 0; i < checkboxes.length; i++) {
                        if (!checkboxes[i].checked) {
                            checkboxes[i].parentNode.classList.add('unavailable');
                        }
                    }
                }
            } else {
                for (let i = 0; i < checkboxes.length; i++) {
                    checkboxes[i].parentNode.classList.remove('unavailable');
                }                    
                playBtn.classList.add('unavailable');
            }
        });
        return checkElement;
    }
}

export default RootFighterView;
